unalias -a
alias more='less'
alias ocaml='rlwrap ocaml'
alias less='less -R'
alias ?='d'
alias ??=googl
alias ls='ls -h --color=auto'
alias ll="ls -la"
alias vi=nvim
alias py=python3
alias l.='ls -d .* --color=tty'
alias c='clear'
alias mv='mv -n'
alias clear='echo ${clear}'
#alias cds='. cds &>/dev/null'
alias notes='. notes'
alias rzi='rm -rf **/*Zone.Identifier'
alias lynx='~/repos/gitlab.com/snapplesacue/dotfiles/scripts/lynx/lynx'

